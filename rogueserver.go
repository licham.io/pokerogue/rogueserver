/*
	Copyright (C) 2024  Pagefault Games

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"encoding/gob"
	"flag"
	"log"
	"net"
	"net/http"
	"os"

	"github.com/pagefaultgames/rogueserver/api"
	"github.com/pagefaultgames/rogueserver/db"
)

func main() {
	// flag stuff
	debug := flag.Bool("debug", false, "use debug mode")

	proto := parseArg("proto", "protocol for api to use (tcp, unix)", "ROGUESERVER_PROTO", "tcp")
	addr := parseArg("addr", "network address for api to listen on", "ROGUESERVER_ADDR", "0.0.0.0:8001")
	tlscert := parseArg("tlscert", "tls certificate path", "ROGUESERVER_TLS_CERT", "")
	tlskey := parseArg("tlskey", "tls key path", "ROGUESERVER_TLS_KEY", "")

	dbuser := parseArg("dbuser", "database username", "ROGUESERVER_DB_USER", "pokerogue")
	dbpass := parseArg("dbpass", "database password", "ROGUESERVER_DB_PASS", "pokerogue")
	dbproto := parseArg("dbproto", "protocol for database connection", "ROGUESERVER_DB_PROTO", "tcp")
	dbaddr := parseArg("dbaddr", "database address", "ROGUESERVER_DB_ADDR", "localhost")
	dbname := parseArg("dbname", "database name", "ROGUESERVER_DB_NAME", "pokeroguedb")

	flag.Parse()

	// register gob types
	gob.Register([]interface{}{})
	gob.Register(map[string]interface{}{})

	// get database connection
	err := db.Init(*dbuser, *dbpass, *dbproto, *dbaddr, *dbname)
	if err != nil {
		log.Fatalf("failed to initialize database: %s", err)
	}

	// create listener
	listener, err := createListener(*proto, *addr)
	if err != nil {
		log.Fatalf("failed to create net listener: %s", err)
	}

	mux := http.NewServeMux()

	// init api
	if err := api.Init(mux); err != nil {
		log.Fatal(err)
	}

	// start web server
	handler := prodHandler(mux)
	if *debug {
		handler = debugHandler(mux)
	}

	if *tlscert == "" {
		err = http.Serve(listener, handler)
	} else {
		err = http.ServeTLS(listener, handler, *tlscert, *tlskey)
	}
	if err != nil {
		log.Fatalf("failed to create http server or server errored: %s", err)
	}
}

func parseArg(flagName string, flagDescription string, varName string, fallbackValue string) *string {
	varValue, varExists := os.LookupEnv(varName)
	if !varExists {
		return flag.String(flagName, fallbackValue, flagDescription)
	} else {
		return flag.String(flagName, varValue, flagDescription)
	}
}

func createListener(proto, addr string) (net.Listener, error) {
	if proto == "unix" {
		os.Remove(addr)
	}

	listener, err := net.Listen(proto, addr)
	if err != nil {
		return nil, err
	}

	if proto == "unix" {
		if err := os.Chmod(addr, 0777); err != nil {
			listener.Close()
			return nil, err
		}
	}

	return listener, nil
}

func prodHandler(router *http.ServeMux) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type")
		w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST")
		w.Header().Set("Access-Control-Allow-Origin", "https://pokerogue.queermunity.licham.io")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		router.ServeHTTP(w, r)
	})
}

func debugHandler(router *http.ServeMux) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		router.ServeHTTP(w, r)
	})
}
